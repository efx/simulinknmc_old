function AFOFS_loadParam(modelName, dimension)
    mdl = modelName;
    load_system(mdl);
    hws = get_param(bdroot, 'modelworkspace');

    AFOFS_DIM = Simulink.Parameter;
    AFOFS_DIM.Value = dimension;
    AFOFS_DIM.DataType = 'double';
    
    hws.assignin('AFOFS_DIM',AFOFS_DIM);

end