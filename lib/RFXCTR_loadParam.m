% 
% NMS_Model_MechInit.m  - Set mechanical parameters of the model.
%
%    This parameter setup includes: 
%    1. segment geometries, masses and inertias,
%    2. muscle-skeleton mechanical links,
%    3. muscle mechanics, and
%    4. ground interaction.
%
% H.Geyer
% 5 October 2006
%


% gravity
g = 9.81;




% % ---------------------
% % 1.6 Joint Soft Limits
% % ---------------------
% 
% % angles at which soft limits engages
phi12_LowLimit =  70*pi/180; %[rad]
phi12_UpLimit  = 130*pi/180; %[rad]
% 
% phi23_UpLimit  = 175*pi/180; %[rad]
% 
% phi34_UpLimit  = 230*pi/180; %[rad]
% 
% % soft block reference joint stiffness
 c_jointstop     = 0.3 / (pi/180);  %[Nm/rad]
% 
% % soft block maximum joint stop relaxation speed
 w_max_jointstop = 1 * pi/180; %[rad/s]
% 
% 
% 
% 
% 
% % ****************************** %
% % 2. BIPED MUSCLE-SKELETON-LINKS %
% % ****************************** %

% ----------------------------------------
% 2.1 Ankle Joint Specific Link Parameters
% ----------------------------------------

% SOLeus attachement
rSOL       =       0.05; % [m] radius 
phimaxSOL  = 110*pi/180; % [rad] angle of maximum lever contribution
phirefSOL  =  80*pi/180; % [rad] reference angle at which MTU length equals 
rhoSOL     =        0.5; %       sum of lopt and lslack 

% Tibialis Anterior attachement
rTA       =        0.04; % [m]   constant lever contribution 
phimaxTA   =  80*pi/180; % [rad] angle of maximum lever contribution
phirefTA   = 100*pi/180; % [rad] reference angle at which MTU length equals 
rhoTA      =        0.7; 

                         

% ************************* %
% 3. BIPED MUSCLE MECHANICS %
% ************************* %

% -----------------------------------
% 3.1 Shared Muscle Tendon Parameters
% -----------------------------------

% excitation-contraction coupling
preA =  0.01; %[] preactivation
tau  =  0.01; %[s] delay time constant

% contractile element (CE) force-length relationship
w    =   0.56; %[lopt] width
c    =   0.05; %[]; remaining force at +/- width

% CE force-velocity relationship
N    =   1.5; %[Fmax] eccentric force enhancement
K    =     5; %[] shape factor

% Series elastic element (SE) force-length relationship
eref =  0.04; %[lslack] tendon reference strain



% ------------------------------
% 3.2 Muscle-Specific Parameters
% ------------------------------

% soleus muscle
FmaxSOL    = 4000; % maximum isometric force [N]
loptSOL    = 0.04; % optimum fiber length CE [m]
vmaxSOL    =    6; % maximum contraction velocity [lopt/s]
lslackSOL  = 0.27; % tendon slack length [m] 0.26


% tibialis anterior
FmaxTA     =  800; % maximum isometric force [N]
loptTA     = 0.06; % optimum fiber length CE [m]
vmaxTA     =   12; % maximum contraction velocity [lopt/s]
lslackTA   = 0.23; % tendon slack length [m] 0.23











