function y = morphG(varargin)
if(nargin < 3)
    T = 1;
else
    T = varargin{3};
end

persistent s t knot;
if( isempty(s))
    knot = length(varargin{1});
    t = linspace(0,T,knot);
    s=spline(t,varargin{1});
    
end

y = ppval(s,mod(varargin{2},1));
end