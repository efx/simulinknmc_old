classdef CpgSignalManager<handle
   properties
      SignalsName;
      Signals;

   end
   methods
        function obj = CpgSignalManager()
            obj.SignalsName = struct;
            obj.Signals = struct;

        end
        %function r = roundOff(obj)
        %    r = round([obj.Value],2);
        %end
        function value = getCurrentState(obj)
            field = get_param(gcs, 'SaveName');
            value = obj.Signals.(field).Enabled;
        end
        function value = getCurrentValue(obj)
            field = get_param(gcs, 'SaveName');
            value = obj.Signals.(field).Value;
        end
        function obj = initCurrent(obj)
            field = get_param(gcb, 'SaveName');
            try
                evalin('base',[field ';']);
            catch
                evalin('base',[field ' = 0;']);
            end
            try
                obj.Signals.(field).Enabled;
            catch
                obj.Signals.(get_param(gcb,'SaveName')).Value = zeros(1,1000);
                obj.Signals.(get_param(gcb,'SaveName')).Enabled = 0;
                obj.SignalsName.(get_param(gcb,'SaveName')) = 1;
            end
        end
        function obj = initCPG_automaticName(obj)
            set_param(gcb,'SaveName', matlab.lang.makeValidName(gcb));
        end
        % Function called when the mask name is changed
        %    tries to access the enable feature of the CPG block
        %    if it can not it creates a new structure for the new signal
        %    if it can and the signal is not enable it initialize it.
        function obj = initCPG(obj)
            if(~strcmp(get_param([gcb '/CPG/LoadFromWorkspace'],'VariableName') ,['CPG.Signals.' get_param(gcb,'SaveName') '.Value']))
                try
                    if(~obj.Signals.(get_param(gcb,'SaveName')).Enabled)
                        obj.Signals.(get_param(gcb,'SaveName')).Value = zeros(1,1000);
                        obj.Signals.(get_param(gcb,'SaveName')).Enabled = 0;
                        evalin('base',[get_param(gcb,'SaveName') ' = 0;']);
                        obj.SignalsName.(get_param(gcb,'SaveName')) = 1;
                    end
                catch
                    obj.Signals.(get_param(gcb,'SaveName')).Value = zeros(1,1000);
                    obj.Signals.(get_param(gcb,'SaveName')).Enabled = 0;
                    evalin('base',[get_param(gcb,'SaveName') ' = 0;']);
                    obj.SignalsName.(get_param(gcb,'SaveName')) = 1;
                end
            end            
            set_param([gcb '/CPG/LoadFromWorkspace'],'VariableName',['CPG.Signals.' get_param(gcb,'SaveName') '.Value']);
            set_param([gcb '/GenerateAndSaveNominalShape/SaveToWorkspace/saveGto'],'VariableName',get_param(gcb,'SaveName'));
            set_param([gcb '/AlreadySavedInWorkspace'],'Value',['CPG.Signals.' get_param(gcb,'SaveName') '.Enabled']);

        end
        % Function called at the end of the simulation by each CPG block
        % It saves the extracted signals to the CPG structure
        function obj = saveCurrent(obj)
            field = get_param(gcb, 'SaveName');
            if(isstruct(evalin('base',field)))
                if(~isempty(evalin('base',[field '.signals.values'])))
                    obj.Signals.(field).Value = evalin('base',[field '.signals.values']);
                    obj.Signals.(field).Enabled = 1;
                end
                evalin('base',['clear ' field ]);
            else
                disp(['error saving "' field  '", seems unused']);
            end
        end
        function obj = saveAll(obj)
            fields = fieldnames(obj.SignalsName);
            for i = 1:numel(fields)
              if(isstruct(evalin('base',fields{i})))
                obj.Signals.(fields{i}).Value = evalin('base',[fields{i} '.signals.values']);
                obj.Signals.(fields{i}).Enabled = 1;
              end
            end
        end
   end
end