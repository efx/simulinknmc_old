function MORPHOSC_loadParam(modelName, g)
    mdl = modelName;
    load_system(mdl);
    hws = get_param(bdroot, 'modelworkspace');

    MORPHOSC_GFunc = Simulink.Parameter;
    MORPHOSC_GFunc.Value = g;
    %MORPHOSC_GFunc.Dimension = length(g);
    MORPHOSC_GFunc.DataType = 'double';
    
    hws.assignin('MORPHOSC_GFunc',MORPHOSC_GFunc);

end