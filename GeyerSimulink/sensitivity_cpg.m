%% INIT for cpg_record_test.slx
clear all;
%addpath('./lib');
load('g_shape')
load('CPG_Data')


%% sensitivity
alphaFmGlu=1;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutFmGlu = sim(model, model_cs);
save('log_cpg_FmGlu','simOutFmGlu')
alphaFmGlu=0;

alphaFmHam=1;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutFmHam = sim(model, model_cs);
save('log_cpg_FmHam','simOutFmHam')
alphaFmHam=0;

alphaFmSol=1;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutFmSol = sim(model, model_cs);
save('log_cpg_FmSol','simOutFmSol')
alphaFmSol=0;

alphaFmVas=1;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutFmVas = sim(model, model_cs);
save('log_cpg_FmVas','simOutFmVas')
alphaFmVas=0;


alphaLceHam=1;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutLceHam = sim(model, model_cs);
save('log_cpg_LceHam','simOutLceHam')
alphaLceHam=0;

alphaLceHfl=1;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutLceHfl = sim(model, model_cs);
save('log_cpg_LceHfl','simOutLceHfl')
alphaLceHfl=0;

alphaLceTa=1;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutLceTa = sim(model, model_cs);
save('log_cpg_LceTa','simOutLceTa')
alphaLceTa=0;

alphaLceTa1=1;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutLceTa1 = sim(model, model_cs);
save('log_cpg_LceTa1','simOutLceTa1')
alphaLceTa1=0;

alphaFmGas=1;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutFmGas = sim(model, model_cs);
save('log_cpg_FmGas','simOutFmGas')
alphaFmGas=0;






