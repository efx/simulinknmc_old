%% INIT for cpg_record_test.slx
clear all;
%addpath('./lib');
load('g_shape')
load('CPG_Data')

%% amp
 alphaFmHam=0.5; alphaLceHfl=0.5; amp_var=[0 1 0];
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutAmpUpH = sim(model, model_cs);
save('log_cpg_AmpUpH','simOutAmpUpH')
alphaFmHam=0; alphaLceHfl=0;

% amp
 alphaFmSol=0.5; alphaFmGas=0.5;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutAmpUpA = sim(model, model_cs);
save('log_cpg_AmpUpA','simOutAmpUpA')
alphaFmSol=0; alphaFmGas=0; amp_var=[1 0 0];

%% amp
 alphaFmHam=0.5; alphaLceHfl=0.5; amp_var=[0 0 1];
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')

cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutAmpDownH = sim(model, model_cs);
save('log_cpg_AmpDownH','simOutAmpDownH')
alphaFmHam=0; alphaLceHfl=0;

% amp
 alphaFmSol=0.5; alphaFmGas=0.5;
model = 'new_nms_model';
load_system(model)
simMode = get_param(model, 'SimulationMode');
set_param(model, 'SimulationMode', 'accelerator')
cs = getActiveConfigSet(model);
model_cs = cs.copy;
set_param(model_cs,'AbsTol','1e-5',...
         'SaveState','on','StateSaveName','xoutNew',...
         'SaveOutput','on','OutputSaveName','youtNew')
simOutAmpDownA = sim(model, model_cs);
save('log_cpg_AmpDownA','simOutAmpDownA')
alphaFmSol=0; alphaFmGas=0;amp_var=[1 0 0];
