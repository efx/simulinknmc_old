%%
gLFmGas=gLFmGas.data(:,:,end);
gRFmGas=gRFmGas.data(:,:,end);

gLFmGlu=gLFmGlu.data(:,:,end);
gRFmGlu=gRFmGlu.data(:,:,end);

gLFmHam=gLFmHam.data(:,:,end);
gRFmHam=gRFmHam.data(:,:,end);

gLFmSol=gLFmSol.data(:,:,end);
gRFmSol=gRFmSol.data(:,:,end);

gLFmVas=gLFmVas.data(:,:,end);
gRFmVas=gRFmVas.data(:,:,end);

gLLceHam=gLLceHam.data(:,:,end);
gRLceHam=gRLceHam.data(:,:,end);

gLLceHfl=gLLceHfl.data(:,:,end);
gRLceHfl=gRLceHfl.data(:,:,end);

gLLceTa=gLLceTa.data(:,:,end);
gRLceTa=gRLceTa.data(:,:,end);

gLLceTa1=gLLceTa1.data(:,:,end);
gRLceTa1=gRLceTa1.data(:,:,end);

%%
save('g_shape','gLFmGas','gRFmGas','gLFmGlu','gRFmGlu','gLFmHam','gRFmHam','gLFmSol','gRFmSol','gLFmVas','gRFmVas',...
    'gLLceHam','gRLceHam','gLLceHfl','gRLceHfl','gLLceTa','gRLceTa','gLLceTa1','gRLceTa1')
%% 

save('log_data_normal','dxThight','KneeState','phidphiJointL','phidphiJointR','RonLLonR','Stance','TauJointL','TauJointR','TauMuscleL','TauMuscleR',...
    'ThetaDtheta','xHatdxHat','xHipdxHip')