%% INIT for cpg_record_test.slx
clear all;
addpath('./lib');
load('extern_data')

N_record=Simulink.Parameter;
N_start=Simulink.Parameter;

N_start.Value=2;
N_record.Value=5;

swing=-1; stance=1;
left=1; right=-1;

%% LOAD CPG DATA
if(exist('CPG_Signal_Data.mat'))
    load('CPG_Signal_Data');
else
    CPG = CpgSignalManager();
end

%% SAVE CPG DATA
save('CPG_Signal_Data','CPG');


%% init for examples_testSimulinkParam.slx
mdl = 'examples_testSimulinkParam';

gshapes = load('g_data');

AFOFS_loadParam(mdl,30);
MORPHOSC_loadParam(mdl,gshapes.g_left_ham);

%simMode = get_param(mdl, 'SimulationMode');




